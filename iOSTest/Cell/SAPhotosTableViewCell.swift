//
//  SAPhotosTableViewCell.swift
//  iOSTest
//
//  Created by Andrey Sokolov on 07.12.2017.
//  Copyright © 2017 Sasin Andrey. All rights reserved.
//

import UIKit

class SAPhotosTableViewCell: UITableViewCell {

    @IBOutlet weak var photosImages: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

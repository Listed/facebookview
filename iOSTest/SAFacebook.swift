
//
//  SAFacebook.swift
//  iOSTest
//
//  Created by Andrey Sokolov on 07.12.2017.
//  Copyright © 2017 Sasin Andrey. All rights reserved.
//

import Foundation
import FacebookCore
import FacebookLogin
import FBSDKLoginKit

var dict : [String : AnyObject]!

func jediGreet(name: String, ability: String) -> (error: String, mayTheForceBeWithYou: String) {
    return ("Good bye, \(name)." as! Error as! String, " May the \(ability) be with you.")
}

func loginFaceBook(completionHandler: @escaping (_ error: Any?, _ dict: Any?) -> Void){
    
    if (FBSDKAccessToken.current()) == nil{
        let loginManager = LoginManager()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SALoginViewController")
        
        loginManager.logIn(readPermissions: [.publicProfile,.userPhotos], viewController: controller) { loginResult in
            switch loginResult {
            case .failed(let error):
                return completionHandler(error, nil)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, _):
                getFBAlbums(completionHandler: {(dict, error) in
                    if(error == nil){
                        return completionHandler(error, dict)
                    }else{
                        return completionHandler(error, nil)
                    }
                })
            }
        }
    }else{
        getFBAlbums(completionHandler: {(dict, error) in
            if(error == nil){
                return completionHandler(error, dict)
            }else{
                return completionHandler(error, nil)
            }
        })
    }
}

func getFBAlbums(completionHandler: @escaping (_ dict: [String : AnyObject], _ error: Any?) -> Void){
    FBSDKGraphRequest(graphPath: "/me/albums", parameters: ["fields": "id, name, source"]).start(completionHandler: { (connection, result, error) -> Void in
        dict = result as! [String : AnyObject]
        if (error == nil){
            return completionHandler(dict, nil)
        }else{
            return completionHandler(dict, error)
        }
    })

}

func getImageUrlFromAlbums(idString:String,completionHandler: @escaping (_ array: NSMutableArray?) -> Void){
    FBSDKGraphRequest(graphPath: "/\(idString)/photos", parameters: ["fields": "images"]).start(completionHandler: { (connection, result, error) -> Void in
        var dictPhotos : [String : AnyObject]!
        dictPhotos = result as! [String : AnyObject]
        let arraySourceURL : NSMutableArray = []
        
        if (error == nil){
            for data in dictPhotos["data"] as! NSArray{
                let dataDict: NSArray = ((data as! NSDictionary) as! [String : AnyObject])["images"] as! NSArray
                let dataString: String  =  (dataDict[0] as! [String : AnyObject])["source"] as! String
                arraySourceURL.add(dataString)
            }
            return completionHandler(arraySourceURL)
        }else{
            return completionHandler(nil)
        }
    })
    
}

//
//  SAAlbumsViewController.swift
//  iOSTest
//
//  Created by Andrey Sokolov on 07.12.2017.
//  Copyright © 2017 Sasin Andrey. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
}

class SAAlbumsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
    var allAlbums:NSMutableArray = []
    var albumsSelect:NSMutableArray = []
    var dictAlbums: NSMutableArray = []
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dictAlbums.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let albums = self.dictAlbums[indexPath.row] as? SAAlbums
        let cell = tableView.dequeueReusableCell(withIdentifier: "SAAlbumTableViewCell") as? SAAlbumTableViewCell!
        cell!.labelAlbums?.text = albums?.titleAlbums
        
        getImageUrlFromAlbums(idString: (albums?.idAlbum!)!,completionHandler: {(urlArray) in
            if(urlArray != nil){
                self.allAlbums.add(urlArray!)
                cell!.imageViewAlbums?.downloadedFrom(link: urlArray?.object(at: 0) as! String)
            }
        })
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 240
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if(self.allAlbums.count > indexPath.row){
            let index:Int = indexPath.row
            self.albumsSelect = self.allAlbums.object(at: index) as! NSMutableArray
            self.performSegue(withIdentifier: "SAPhotosViewController", sender: self)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: Seque
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "SAPhotosViewController") {
            
            let albumsViewController = (segue.destination as! SAPhotosViewController)
            albumsViewController.photos = albumsSelect
        }
    }
}

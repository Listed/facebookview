//
//  ViewController.swift
//  iOSTest
//
//  Created by Andrey Sokolov on 07.12.2017.
//  Copyright © 2017 Sasin Andrey. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class SALoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var buttonLogin: UIButton!
    
    var dictAlbums : [String : AnyObject]!
    var arrayAlbums: NSMutableArray = []
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Handle clicks on the button
        buttonLogin.addTarget(self, action:#selector(self.tapButtonLogin), for: .touchUpInside)
        
        //if the user is already logged in
        if (FBSDKAccessToken.current()) != nil{
            getFBAlbums(completionHandler: {(dict, error) in
                if((error) != nil){
                    print(error as Any)
                }else{
                    self.dictAlbums = dict 
                    
                    for dictMy in self.dictAlbums["data"] as! NSArray{
                        self.arrayAlbums.add(SAAlbums(fromDict: dictMy as! [String:AnyObject]))
                    }
                    
                    self.performSegue(withIdentifier: "SAAlbumsViewController", sender: self)
                    
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Action Button
    @objc func tapButtonLogin() {
        loginFaceBook(completionHandler: {(error, dict) in
            if((error) != nil){
                print(error as Any)
            }else{
                self.dictAlbums = dict as! [String : AnyObject]
            
                for dictMy in self.dictAlbums["data"] as! NSArray{
                    self.arrayAlbums.add(SAAlbums(fromDict: dictMy as! [String:AnyObject]))
                }
    
                self.performSegue(withIdentifier: "SAAlbumsViewController", sender: self)
                
            }
        })
    }
    
    //MARK: Seque
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "SAAlbumsViewController") {
            
            let albumsViewController = (segue.destination as! SAAlbumsViewController)
            albumsViewController.dictAlbums = self.arrayAlbums
        }
    }
}


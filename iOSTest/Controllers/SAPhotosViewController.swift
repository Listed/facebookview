//
//  SAPhotosViewController.swift
//  iOSTest
//
//  Created by Andrey Sokolov on 07.12.2017.
//  Copyright © 2017 Sasin Andrey. All rights reserved.
//

import UIKit

class SAPhotosViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var photos: NSMutableArray!
    var url:String!
    
    //MARK:Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: TableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let url = self.photos[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SAPhotosTableViewCell") as? SAPhotosTableViewCell!
        
        cell!.photosImages?.downloadedFrom(link: url as! String)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 240
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let index:Int = indexPath.row
        self.url = self.photos.object(at: index) as! String
        self.performSegue(withIdentifier: "SAImagesViewController", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: Seque
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "SAImagesViewController") {
            
            let albumsViewController = (segue.destination as! SAImagesViewController)
            albumsViewController.url = self.url
        }
    }
}

//
//  SAAlbums.swift
//  iOSTest
//
//  Created by Andrey Sokolov on 07.12.2017.
//  Copyright © 2017 Sasin Andrey. All rights reserved.
//

import UIKit

class SAAlbums: NSObject {
    
    var titleAlbums: String?
    var idAlbum: String?
    
    init(fromDict dict: [String:AnyObject]) {
        titleAlbums = dict["name"] as? String
        idAlbum = dict["id"] as? String
    }
    
}
